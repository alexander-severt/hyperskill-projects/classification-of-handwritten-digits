import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils._testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning
from sklearn.preprocessing import Normalizer
from sklearn.model_selection import GridSearchCV

# Load the data
(full_X_train, full_y_train), (full_X_test, full_y_test) = tf.keras.datasets.mnist.load_data(path="mnist.npz")

# Reshape the feature and target data
X = full_X_train.reshape(full_X_train.shape[0], full_X_train.shape[1] * full_X_train.shape[2])
y = full_y_train

# Filter to use only the first 6000 samples
sample_num = 6000
X = X[:sample_num]
y = y[:sample_num]

# Split the data into train and test data
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=40)

# Normalize the features data
X_train_norm = Normalizer().transform(X_train)
X_test_norm = Normalizer().transform(X_test)

# List the parameters for tuning
params_knn = {
            'n_neighbors': [3, 4],
            'weights': ['uniform', 'distance'],
            'algorithm': ['auto', 'brute']
        }
params_rfc = {
            'n_estimators': [800],
            'max_features': ['auto', 'log2'],
            'class_weight': ['balanced', 'balanced_subsample'],
            'random_state': [40]
        }

# List the different types of models
search_knn = GridSearchCV(KNeighborsClassifier(), params_knn, scoring='accuracy', n_jobs=-1)\
    .fit(X_train_norm, y_train)
search_rfc = GridSearchCV(RandomForestClassifier(), params_rfc, scoring='accuracy', n_jobs=-1)\
    .fit(X_train_norm, y_train)

@ignore_warnings(category=ConvergenceWarning)
def fit_predict_eval(model, features_train, features_test, target_train, target_test):
    mdl = model.fit(features_train, target_train)
    scr = mdl.score(features_test, target_test)
    if type(mdl).__name__ == 'KNeighborsClassifier':
        print('K-nearest neighbours algorithm')
    else:
        print('Random forest algorithm')
    print(f'best estimator: {mdl}\naccuracy: {scr.round(3)}\n')


fit_predict_eval(KNeighborsClassifier(**search_knn.best_params_), X_train_norm, X_test_norm, y_train, y_test)
fit_predict_eval(RandomForestClassifier(**search_rfc.best_params_), X_train_norm, X_test_norm, y_train, y_test)
